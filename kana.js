var kana = (function(window, document, undefined) {

    'use strict';

    var LOGO_CANV = document.getElementById("kanaCanvas");
    var REF_CANV = document.createElement('canvas');
    var WIDTH = REF_CANV.width = LOGO_CANV.width;
    var HEIGHT = REF_CANV.height = LOGO_CANV.height;

    var REF_CTX = REF_CANV.getContext('2d');
    var LOGO_CTX = LOGO_CANV.getContext('2d');
    var BOUND_X = LOGO_CANV.getBoundingClientRect().left;
    var BOUND_Y = LOGO_CANV.getBoundingClientRect().top;

    var TARGET; //For touch events

    var Particles = [];

    var Environment = {
        friction: 0,
        size: 0,
        spacing: 0,
        ease: 0,
        color: "#FFFFFF"
    };

    var Particle;

    var setParticleObject = function(constructor_func) {
        if (typeof(constructor_func) === "function") {
            Particle = constructor_func;
        } else {
            throw new Error("In Kana.setParticleObject: Please pass a constructor function as argument.");
        }
    };

    var setUpdateParticleMethod = function(update_func) {
        if (typeof(update_func) === "function") {
            Particle.prototype.update = update_func;
        } else {
            throw new Error("In Kana.setUpdateParticleMethod: Please pass a function as argument.");
        }
    };

    var getCanvasDimensions = function() {
        return {
            width: WIDTH,
            height: HEIGHT
        };
    };

    var setFriction = function(friction) {
        if (typeof(friction) === "number") {
            Environment.friction = friction;
        } else {
            throw new Error("In Kana.setFriction: friction should be of type number.");
        }
    };

    var setSize = function(size) {
        if (typeof(size) === "number") {
            Environment.size = size;
        } else {
            throw new Error("Kana.setSize: size should be of type number.");
        }
    };

    var setSpacing = function(spacing) {
        if (typeof(spacing) === "number") {
            Environment.spacing = spacing;
        } else {
            throw new Error("Kana.setSpacing: spacing should be of type number.");
        }
    };

    var setEase = function(ease) {
        if (typeof(ease) === "number") {
            Environment.ease = ease;
        } else {
            throw new Error("Kana.setEase: ease should be of type number.");
        }
    };

    var setColor = function(color) {
        if (color.match(/#([a-f0-9]{3}){1,2}\b/i) !== null) {
            Environment.color = color;
        } else {
            throw new Error("Kana.setColor: color should be a valid hex color value of type string.");
        }
    };

    var getEnvironment = function() {
        return {
            friction: Environment.friction,
            size: Environment.size,
            spacing: Environment.spacing,
            ease: Environment.ease,
            color: Environment.color
        };
    };

    var setEnvironment = function(friction, size, spacing, ease, color) {
        if (typeof(friction) === "number" && typeof(size) === "number" && typeof(spacing) === "number" && typeof(ease) === "number" && color.match(/#([a-f0-9]{3}){1,2}\b/i) !== null) {
            Environment.friction = friction;
            Environment.size = size;
            Environment.spacing = spacing;
            Environment.ease = ease;
            Environment.color = color;
            LOGO_CTX.fillStyle = color;
        } else {
            //console.log(typeof(friction), typeof(size), typeof(spacing), typeof(ease));
            throw new Error("In Kana.setEnvironment: invalid argument types.");
        }
    };

    var Mouse = {
        x: 0,
        y: 0,
        radius: 0
    };

    var setMouseCoordinates = function(x, y) {
        if (typeof(x) === "number" && typeof(y) === "number") {
            Mouse.x = x;
            Mouse.y = y;
        } else {
            throw new Error("In setMouseCoordinates: Coordinates should be of type number");
        }
    };

    var getMouse = function() {
        return {
            x: Mouse.x,
            y: Mouse.y,
            radius: Mouse.radius
        };
    };

    var setRadius = function(radius) {
        if (typeof(radius) === "number") {
            Mouse.radius = radius;
        } else {
            throw new Error("Radius should be of type number");
        }
    };

    var setMouse = function(x, y, radius) {
        if (typeof(x) !== "number" || typeof(y) !== "number" || typeof(radius) !== "number") {
            throw new Error("In Kana.setMouse: Invalid argument types.");
        } else {
            if (x)
                Mouse.x = x;
            if (y)
                Mouse.y = y;
            if (radius)
                Mouse.radius = radius;
        }
    };

    var Text = {
        value: "Kana JS",
        weight: "bold",
        size: "",
        font: "Arial",
        style: "96px Arial"
    };

    var setValue = function(value) {
        if (typeof(value) === "string") {
            Text.value = value;
        } else {
            throw new Error("Invalid argument types.");
        }
    };

    var setText = function(value, weight, size, font) {
        if (typeof(value) === "string" && typeof(weight) === "string" &&
            (typeof(size) === "number" || typeof(size) === "string") && typeof(font) === "string") {
            Text.value = value;
            Text.weight = weight;
            Text.size = (size).toString() + "px";
            Text.font = font;
            Text.style = weight + " " + size + "px" + " " + font;
        } else {
            throw new Error("In Kana.setText: Invalid argument types.");
        }
    };

    //Functions:
    var parseImageData = function(data) {
        var particles = [];
        var index;
        for (var y = 0; y < HEIGHT; y += Environment.spacing) {
            for (var x = 0; x < WIDTH; x += Environment.spacing) {
                index = (y * WIDTH + x) * 4;
                if (data[--index] > 0) {
                    particles.push(new Particle(x, y));
                }
            }
        }

        return particles;
    };

    var particlize = function(alignment, baseline, x, y) {
        REF_CTX.font = Text.style;
        REF_CTX.textAlign = alignment;
        REF_CTX.textBaseline = baseline;
        REF_CTX.imageSmoothingEnabled = true;
        REF_CTX.fillText(Text.value, x, y);
        Particles = parseImageData(REF_CTX.getImageData(0, 0, WIDTH, HEIGHT).data);

        //Accounting for potential changes to the document:
        recalculateBounds();
    };

    var update = function() {
        for (var i = 0; i < Particles.length; ++i) {
            var p = Particles[i];
            p.update();
        }
    };

    var render = function() {
        LOGO_CTX.clearRect(0, 0, WIDTH, HEIGHT);

        for (var i = 0; i < Particles.length; ++i) {
            var p = Particles[i];
            LOGO_CTX.fillRect(p.x, p.y, Environment.size, Environment.size);
        }
    };

    var getCanvas = function() {
        return LOGO_CANV;
    };

    var addWindowResizeHandler = function(func, opt) {
        if (typeof func === "function") {

            var func_new = function() {
                func();
                BOUND_X = LOGO_CANV.getBoundingClientRect().left;
                BOUND_Y = LOGO_CANV.getBoundingClientRect().top;
            };
            window.onresize = func_new;
        } else {
            throw new Error("In Kana.addWindowResizeHandler: Invalid argument types.");
        }
    };

    var getCanvasBounds = function() {
        return {
            BOUND_X: BOUND_X,
            BOUND_Y: BOUND_Y
        }
    };

    var recalculateBounds = function() {
        BOUND_X = LOGO_CANV.getBoundingClientRect().left;
        BOUND_Y = LOGO_CANV.getBoundingClientRect().top;
    }

    //Expose required objects:
    var Kana = {
        setEnvironment: setEnvironment,
        setFriction: setFriction,
        setEase: setEase,
        setSize: setSize,
        setSpacing: setSpacing,
        setParticleObject: setParticleObject,
        setUpdateParticleMethod: setUpdateParticleMethod,
        setColor: setColor,
        getEnvironment: getEnvironment,
        setMouse: setMouse,
        setMouseCoordinates: setMouseCoordinates,
        setRadius: setRadius,
        getMouse: getMouse,
        setValue: setValue,
        setText: setText,
        particlize: particlize,
        canvasUpdate: update,
        canvasRender: render,
        getCanvas: getCanvas,
        addWindowResizeHandler: addWindowResizeHandler,
        getCanvasDimensions: getCanvasDimensions,
        update: update,
        render: render,
        getCanvasBounds: getCanvasBounds,
        recalculateBounds: recalculateBounds
    };
      
    window.Kana = Kana;

})(window, document);