# Kana JS
KanaJS converts text on canvas to pixels and allows addding custom animation and events to text.

Codepen for playing with Kana
- Example: http://codepen.io/rahultri/pen/wWQYyx

There is no proper documentation yet. Please go through kana.js and the example provided for vital details.


Target canvas must have the id "kanaCanvas". Once the script is included
- Use Kana.setText("Hello World", "bold", "96", "Arial"); Will draw "Hello World" of font size 96px(bold) using font Arial. 
- Use Kana.setEnvironment(friction, size, spacing, ease, hexColorCode) to add text color, pixel size, pixel spacing, and friction. Where, friction, size, spacing, and ease are of type number while hexColorCode is of type string.
- Use Kana.setMouse(x, y, radius) to add mouse radius and update mouse position. Where, x, y, and radius are of type number. 
- Kana.setMouseCoordinates() to update just the x and y coordinates.
- Once text and environment are set, use Kana.particlize() to convert text to particles.
- Kana.Particles contains the relative canvas coordinates with alpha > 1.
- Use Kana.setParticleObject() to add custom particle constructor, and Kana.setUpdateParticleMethod() to add a custom update method for animation. View example.js for an example.
- Canvas bounds are calculated on window resize for responsiveness. Any modification to window.onresize() should be done through Kana.addWindowResizeHandler(function, options)
- Kana.getCanvas() returns the target canvas