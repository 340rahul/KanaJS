(function(window, document, undefined) {

    window.onload = function() {
        //loading font using Google WebFont API to ensure fonts are loaded before execution of code:
        var fontScript = document.createElement('script');
        fontScript.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        fontScript.type = 'text/javascript';
        fontScript.async = 'true';
        s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(fontScript, s);

        WebFontConfig = {
            custom: {
                families: ['Jockey One'],
                urls: ['https://fonts.googleapis.com/css?family=Jockey+One']
            },
            //function to be executed after the fonts are loaded:
            active: function() {

                //Event listeners:
                document.getElementById('animate').addEventListener('click', start, false);

                document.getElementById('advanced').addEventListener('click', function() {
                    if (document.getElementById('advancedCheckBox').checked) {
                        document.getElementById('advancedOptions').style.display = 'block';
                    } else {
                        document.getElementById('advancedOptions').style.display = 'none';
                    }
                }, false);

                if (document.getElementById('animationSelect').value == 'default') {
                    document.getElementById('defaultAnimationOptions').style.display = 'block';
                }

                document.getElementById('animationSelect').addEventListener('change', function() {

                }, false);

                document.getElementById('resetBtn').addEventListener('click', function() {
                    if (document.getElementById('animationSelect').value == 'default') {
                        document.getElementById('defFrictionInput').value = 0.95;
                        document.getElementById('defSpacingInput').value = 1;
                        document.getElementById('defSizeInput').value = 1;
                        document.getElementById('defEaseInput').value = 0.5;
                        document.getElementById('defMouseRadiusInput').value = 400;
                    }
                }, false);
            }
        }

        function start() {

            //Creating environment, text, and moouse objects:
            var environment = {
                friction: parseInt(document.getElementById('defFrictionInput').value) || 0.95,
                spacing: parseInt(document.getElementById('defSpacingInput').value) || 1,
                size: parseInt(document.getElementById('defSizeInput').value) || 1,
                ease: parseInt(document.getElementById('defEaseInput').value) || 0.5,
                color: parseInt(document.getElementById('colorInput').value) || "#FFFFFF"
            };

            var mouse = {
                x: 0,
                y: 0,
                radius: parseInt(document.getElementById('defMouseRadiusInput').value) || 400
            };

            var text = {
                value: document.getElementById('inputText').value,
                weight: document.getElementById('fontWeightInput').value,
                size: document.getElementById('fontSizeInput').value,
                font: "Jockey One",
            };

            Kana.setEnvironment(environment.friction, environment.size, environment.spacing, environment.ease, environment.color);
            Kana.setMouse(mouse.x, mouse.y, mouse.radius);
            Kana.setText(text.value, text.weight, text.size, text.font);

            var width = Kana.getCanvasDimensions().width;
            var height = Kana.getCanvasDimensions().height;

            var animationStyle = document.getElementById('animationSelect').value;

            //Removing user imput div:
            var element = document.getElementById('userInput');
            element.parentNode.removeChild(element);

            document.getElementById("logoContainer").style.display = "block";

            if (animationStyle == 'default') {

                //custom particle object
                function particle(x, y) {
                    this.x = this.originX = x;
                    this.y = this.originY = y;
                    this.rx = 0,
                        this.ry = 0,
                        this.vx = 0;
                    this.vy = 0;
                    this.force = 0;
                    this.angle = 0;
                    this.distance = 0;
                };

                //custom update method for default animation:
                var update = function() {
                    var mouse = Kana.getMouse();
                    var environment = Kana.getEnvironment();

                    this.rx = mouse.x - (this.x + Kana.getCanvasBounds().BOUND_X);
                    this.ry = mouse.y - (this.y + Kana.getCanvasBounds().BOUND_Y);

                    this.distance = (this.rx * this.rx) + (this.ry * this.ry);
                    this.force = -(mouse.radius) / this.distance;

                    if (this.distance < mouse.radius) {
                        this.angle = Math.atan2(this.ry, this.rx);
                        this.vx += this.force * Math.cos(this.angle);
                        this.vy += this.force * Math.sin(this.angle);
                    }
                    this.x += (this.vx *= environment.friction) + (this.originX - this.x) * environment.ease;
                    this.y += (this.vy *= environment.friction) + (this.originY - this.y) * environment.ease;
                };

                Kana.setParticleObject(particle);
                Kana.setUpdateParticleMethod(update);
                //Particlizing:            
                Kana.particlize("center", "middle", width / 2, height / 2);

                var animate = function() {
                    Kana.update();
                    Kana.render();
                    window.requestAnimationFrame(animate);
                }

                //Adding event handelers:
                Kana.addWindowResizeHandler(function() {}, false);

                var mutationObserver = new MutationObserver(function(mutations) {
                    mutations.forEach(function(mutation) {
                        Kana.recalculateBounds();
                    });
                });
                mutationObserver.observe(document.body, {
                    childList: true,
                    subtree: true
                });

                //Mouse event listeners:
                Kana.getCanvas().addEventListener('mousemove', function(event) {
                    Kana.setMouseCoordinates(event.clientX, event.clientY);
                });

                Kana.getCanvas().addEventListener('mouseleave', function(event) {
                    Kana.setMouseCoordinates(0, 0);
                });

                //Touch event listeners:
                Kana.getCanvas().addEventListener('touchstart', function(event) {
                    Kana.setMouseCoordinates(event.changedTouches[0].clientX, event.changedTouches[0].clientY);
                }, false);

                Kana.getCanvas().addEventListener('touchmove', function(event) {
                    event.preventDefault();
                    Kana.setMouseCoordinates(event.targetTouches[0].clientX, event.targetTouches[0].clientY);
                }, false);

                Kana.getCanvas().addEventListener('touchend', function(event) {
                    event.preventDefault();
                    Kana.setMouseCoordinates(0, 0);
                }, false);

                var target;
                Kana.getCanvas().addEventListener('touchstart', function(event) {
                    event.preventDefault();
                    var touch = event.touches[0];
                    target = document.elementFromPoint(touch.PageX, touch.PageY);
                }, false);

                Kana.getCanvas().addEventListener('touchmove', function(event) {
                    event.preventDefault();
                    var touch = event.touches[0];
                    if (target !== document.elementFromPoint(touch.PageX, touch.PageY)) {
                        //Touch left the target DOM:
                        Kana.setMouseCoordinates(0, 0);
                    }
                }, false);

                animate();
            }
        }
    }
})(window, document);